/*****************************************************
Enoncé du problème :  

Quatre enfants, Anne, Bernard, Claudine et Denis, ayant tous un âge différent 
(entre 4 et 7 ans), jouent chacun à différents endroits avec des billes de 
couleur bleue, jaune, noire ou rouge. On sait de plus que : 

1. Denis joue dans le parc et n’a pas 4 ans, contrairement à
   l’enfant qui a des billes bleues.

2. La fille de 6 ans a des billes jaunes.

3. L’enfant qui joue avec des billes noires est plus âgé que l’enfant qui joue 
   dans le jardin mais plus jeune que Anne.

4. Anne, qui joue dans sa chambre, a 1 an de plus que l’enfant qui joue dans le salon.


(NB : l'énoncé vous est rappelé pour que vous puissiez copier/coller tel quel 
	  les phrases de l'énoncé dans vos modèles)

*********************************************************/

/* Formalisation en tant que problème de satisfaction de contraintes */

X = {
        Anne, Bernard, Claudine, Denis, Salon,Jardin,Parc,Chambre, Rouge, Bleu, Noir,Jaune} //les variables representent tous l'age.



	
D = d^12
	avec d = {4,5,6,7}




C =  {


1. Denis joue dans le parc et n’a pas 4 ans, contrairement à
   l’enfant qui a des billes bleues.


Denis != 4  
Denis =parc
Bleu=4


2. La fille de 6 ans a des billes jaunes.
Jaune=6
Jaune=Anne ou Jaune=Claudine


3. L’enfant qui joue avec des billes noires est plus âgé que l’enfant qui joue 
   dans le jardin mais plus jeune que Anne.

Noir > jardin
Noir<Anne

4. Anne, qui joue dans sa chambre, a 1 an de plus que l’enfant qui joue dans le salon.

Anne = Salon+1
Anne=Chambre


   // TODO 
   // Décrire ici librement les contraintes en faisant précéder chaque contrainte
   // (ou groupe de contraintes) par le texte de l'énoncé qui lui correspond.
   //
   // NB : dans chacun des modèles que vous construirez ultérieurement, on vous 
   // demande de procéder de la même façon, i.e. de recopier chaque portion 
   // du texte initial (sans le déformer) dans un commentaire,
   // suivi immédiatement de la (les) contrainte(s) qui lui correspond(ent).

}
