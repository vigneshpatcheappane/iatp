/*********************************************
 * Modèle OPL Pour le problème des billes
 *
 * Dans ce modèle, chaque variable doit être déclarée explicitement
 * avec son nom d'origine dans le modèle initial
 *
 *********************************************/
using CP;			/* Utilisation du solveur CP-Solver */

/* Déclarations domaines et variables */

range age=4..7;

dvar int Anne in age;
dvar int Bernard in age;
dvar int Claudine in age;
dvar int Denis in age;
dvar int Salon in age;
dvar int Jardin in age;
dvar int Parc in age;
dvar int Chambre in age;
dvar int Rouge in age;
dvar int Bleu in age;
dvar int Noir in age;
dvar int Jaune in age;




/* Paramétrage du solveur */
execute{
    cp.param.searchType="DepthFirst";
    cp.param.workers=1;
    
}


/* Contraintes */

constraints {
/*1. Denis joue dans le parc et n’a pas 4 ans, contrairement à
   l’enfant qui a des billes bleues.*/

    Denis != 4 ; 
    Denis ==Parc;
    Bleu==4;
/*2. La fille de 6 ans a des billes jaunes.*/
    Jaune==6;
    Bernard !=6;
    Denis !=6;
/*3. L’enfant qui joue avec des billes noires est plus âgé que l’enfant qui joue 
   dans le jardin mais plus jeune que Anne.
*/

    Noir > Jardin;
    Noir<Anne;

/*4. Anne, qui joue dans sa chambre, a 1 an de plus que l’enfant qui joue dans le salon.*/

    Anne == Salon+1;
    Anne == Chambre;
}


/* Post-traitement (Affichage Solution) */

	execute {
		writeln("Anne : ",Anne);
	        writeln("Bernard : ",Bernard);
		writeln("Claudine : ",Claudine);
		writeln("Denis : ",Denis);
	}	
   

/*main{

    thisOp1Model.generate();
    cp.startNewSearch();
    while (cp.next()){
        thisOp1Model.postProcess();
    }
}*/






/* {string} variables = {"Anne", "Bernard", "Claudine", "Denis", "Salon","Jardin","Parc","Chambre", "Rouge", "Bleu", "Noir","Jaune"}*/
