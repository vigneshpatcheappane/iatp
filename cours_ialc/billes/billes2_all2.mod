/*********************************************
 * Modèle OPL Pour le problème des billes
 *
 * Utiliser un ou plusieurs tableau pour regrouper les variables 
 * dans des structures adéquates, permettant de simplifier 
 * l'énoncé de certaines contraintes.
 *
 * Faire ne sorte d'afficher toutes les solutions.
 * Mettre le bloc main dans fichier externe.
 *********************************************/
using CP;			/* Utilisation du solveur CP-Solver */

/* Déclarations domaines et variables */
// TODO 
range age=4..7;
{string} prenoms = {"Anne", "Bernard", "Claudine", "Denis"};
{string} lieux = {"Salon","Jardin","Parc","Chambre"};
{string} couleurs = {"Rouge", "Bleu", "Noir","Jaune"};
{string} variables = prenoms union lieux union couleurs;
dvar int solutions[variables] in age;





execute{
    cp.param.searchType="DepthFirst";
    cp.param.workers=1;
    
}


/* Contraintes */

constraints {
/*1. Denis joue dans le parc et n’a pas 4 ans, contrairement à
   l’enfant qui a des billes bleues.*/

     solutions["Denis"] != 4 ; 
     solutions["Denis"] ==solutions["Parc"];
     solutions["Bleu"]==4;
/*2. La fille de 6 ans a des billes jaunes.*/
     solutions["Jaune"]==6;
     solutions["Bernard"] !=6;
     solutions["Denis"] !=6;
/*3. L’enfant qui joue avec des billes noires est plus âgé que l’enfant qui joue 
   dans le jardin mais plus jeune que Anne.
*/

     solutions["Noir"] >  solutions["Jardin"];
     solutions["Noir"]< solutions["Anne"];

/*4. Anne, qui joue dans sa chambre, a 1 an de plus que l’enfant qui joue dans le salon.*/

     solutions["Anne"] ==  solutions["Salon"]+1;
     solutions["Anne"] ==  solutions["Chambre"];

     forall(i, j in prenoms : i < j) {
     	solutions[i]!=solutions[j];
     }	
     forall(i, j in lieux : i < j) {
     	solutions[i]!=solutions[j];
     }	
     forall(i, j in couleurs : i < j) {
     	solutions[i]!=solutions[j];
     }	



}


/* Affichage sollutions*/
	include "../shared/allSolutions.mod";


