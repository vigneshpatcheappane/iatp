/*********************************************
 * Modèle OPL Pour le problème des billes
 *
 * Utiliser un (ou plusieurs) tableau(x) afin de regrouper les variables 
 * dans des structures adéquates.
 * Cela doit permettre de simplifier l'énoncé de certaines contraintes 
 * ainsi que l'affichage de la solution.
 *********************************************/
using CP;			/* Utilisation du solveur CP-Solver */

/* Déclarations domaines et variables */
// TODO 
range age=4..7;
{string} variables = {"Anne", "Bernard", "Claudine", "Denis", "Salon","Jardin","Parc","Chambre", "Rouge", "Bleu", "Noir","Jaune"};
dvar int ages[variables] in age;





execute{
    cp.param.searchType="DepthFirst";
    cp.param.workers=1;
    
}


/* Contraintes */

constraints {
/*1. Denis joue dans le parc et n’a pas 4 ans, contrairement à
   l’enfant qui a des billes bleues.*/

    ages[Denis] != 4 ; 
     ages[Denis] ==Parc;
     ages[Bleu]==4;
/*2. La fille de 6 ans a des billes jaunes.*/
     ages[Jaune]==6;
     ages[Bernard] !=6;
     ages[Denis] !=6;
/*3. L’enfant qui joue avec des billes noires est plus âgé que l’enfant qui joue 
   dans le jardin mais plus jeune que Anne.
*/

     ages[Noir] >  ages[Jardin];
     ages[Noir]< ages[Anne];

/*4. Anne, qui joue dans sa chambre, a 1 an de plus que l’enfant qui joue dans le salon.*/

     ages[Anne] ==  ages[Salon]+1;
     ages[Anne] ==  ages[Chambre];
}


/* Post-traitement (Affichage Solution) */

	execute {
		writeln("Anne : ", Anne);
	        writeln("Bernard : ",Bernard);
		writeln("Claudine : ",Claudine);
		writeln("Denis : ",Denis);
	}	
