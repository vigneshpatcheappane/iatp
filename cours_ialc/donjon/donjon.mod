/*********************************************
 * Modèle pour le problème du carré latin
 *********************************************/
using CP;

/*** Données du problème  ***/
int soldats=12;
 
/*** Variables et domaines  ***/
range r = 0..soldats;

{string} positions = {"ouest", "nord_ouest", "nord", "nord_est", "est", "sud_est", "sud", "sud_ouest"};
dvar int nombre_soldats[positions] in r;


execute{
    cp.param.searchType="DepthFirst";
    cp.param.workers=1;
    
}


/*** Contraintes  ***/

constraints {
	nombre_soldats["ouest"]+nombre_soldats["sud_ouest"]+nombre_soldats["nord_ouest"]>=5;
	nombre_soldats["est"]+nombre_soldats["sud_est"]+nombre_soldats["nord_est"]>=5;
	nombre_soldats["nord"]+nombre_soldats["nord_ouest"]+nombre_soldats["nord_est"]>=5;
	nombre_soldats["sud"]+nombre_soldats["sud_ouest"]+nombre_soldats["sud_est"]>=5;
	sum(p in positions) nombre_soldats[p] == soldats;

	/* contraintes pour la symétrie */
	nombre_soldats["nord_ouest"]<= nombre_soldats["sud_ouest"];
	nombre_soldats["nord_ouest"]<= nombre_soldats["nord_est"];
	nombre_soldats["nord_ouest"]<= nombre_soldats["sud_est"];
	nombre_soldats["sud_est"]<= nombre_soldats["nord_est"];
}
   

/*** Post-traitement  ***/


execute {
		writeln(nombre_soldats["nord_ouest"], "\t", nombre_soldats["nord"], "\t", nombre_soldats["nord_est"]);
		writeln(nombre_soldats["ouest"], "\t \t", nombre_soldats["est"]);
		writeln(nombre_soldats["sud_ouest"], "\t", nombre_soldats["sud"], "\t", nombre_soldats["sud_est"]);


		
	}
include "../shared/allSolutions.mod";
