/*********************************************
 * Modèle pour le problème du carré latin
 *********************************************/
using CP;

/*** Données du problème  ***/
int n=5;
 
/*** Variables et domaines  ***/
range r = 1..n;


dvar int solutions[r][r] in r;



execute{
    cp.param.searchType="DepthFirst";
    cp.param.workers=1;
    
}


/*** Contraintes  ***/
/* pas deux chiffres identique sur la meme ligne et pas deux chiffres identique sur la meme colonne */

constraints {
    forall(i,k,l in r) {
	forall( k,l in r : k<l){
     		solutions[i][k]!=solutions[i][l];
		solutions[k][i]!=solutions[l][i];
	 }
     }	

}
   

/*** Post-traitement  ***/


execute {
		writeln(solutions);
		
	}
include "../shared/allSolutions.mod";
