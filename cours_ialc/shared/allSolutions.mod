/*********************************************
 * Generic main bloc for computing and displaying all solutions
 * as well as the total number of solutions
 *********************************************/
 
main {
	thisOplModel.generate();
	cp.startNewSearch();
	var i=1;
	while (cp.next()) {
		writeln("Solution : ", i);
		thisOplModel.postProcess();
		writeln("_____________________________________________________________________________________________________________________");
		i++;		
	}
}




