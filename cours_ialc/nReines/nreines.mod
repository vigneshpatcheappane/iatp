/*********************************************
 * Modèle pour le problème du carré latin
 *********************************************/
using CP;

/*** Données du problème  ***/
int n=5;
 
/*** Variables et domaines  ***/
range r = 1..n;


/* s[r] = k ou la reine de la ligne r se trouve sur la colonne k*/
dvar int s[r] in r;



execute{
    cp.param.searchType="DepthFirst";
    cp.param.workers=1;
    
}


/*** Contraintes  ***/

constraints {
/* pas deux reines identique sur la meme ligne */
    forall(i,k in r: i<k) {
     		s[i]!=s[k];
	 }
/* pas deux reines identique sur la meme diagonale */
    forall(i,k in r: i<k) {
     		abs(s[i]-s[k])!=abs(i-k);
	 }
}
   

/*** Post-traitement  ***/


execute {
		writeln(s);
		
	}
include "../shared/allSolutions.mod";
