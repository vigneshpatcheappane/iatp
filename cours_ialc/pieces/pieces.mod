using CP;

/*Données du probleme*/

{int} valPieces = {1, 2, 5, 10, 20, 50};
int sommeMaxi = 99;
/*Modele du probleme*/
range r = 1..sommeMaxi;
dvar int nbPieces[val in valPieces ] in 0..(sommeMaxi div val);
dvar int nbPiecesVal[val in valPieces, v in valeurs] in 0 ..(v div val);
dvar int nbTotalDePieces in 0..(sum(val in valPieces) sommeMaxi div val);

execute {
    cp.param.searchType="DepthFirst";
    cp.param.workers=1;
    
}

minimize
	nbTotalPieces;

subject to {
	nbTotalDePieces == sum(val in valPieces) nbPieces[val];
	forall(v in valeurs) {
		v=sum(val in valPieces) v*nbPiecesVal[val,v];
		forall(val in valPieces){
			nbPiecesVal[val,v]<= nbPieces[val];
		}
	}		
}

execute {
	writeln(nbPieces);
}

include "../shared/allSolutions.mod";